import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArchive } from "@fortawesome/free-solid-svg-icons";

import {
  deleteArchivedNote,
  pinArchivedNote,
  unarchiveNote,
  editArchiveNote,
} from "../actions";
import Note from "./Note";
import EditNote from "./EditNote";
import "../styles/archive.scss";

function Archive() {
  const archivedNotes = useSelector((state) => state.archivedNotes);
  const notes = useSelector((state) => state.notes);
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const selectedNoteIndex = params.noteId
    ? archivedNotes.findIndex((n) => n.id === params.noteId)
    : -1;

  useEffect(() => {
    if (params.noteId && selectedNoteIndex === -1) history.push("/archive");
  });

  const deleteNote = (noteId) => {
    dispatch(deleteArchivedNote(archivedNotes, noteId), shallowEqual);
  };

  const togglePin = (note) => {
    dispatch(pinArchivedNote(archivedNotes, notes, note), shallowEqual);
  };

  const toggleArchive = (note) => {
    dispatch(unarchiveNote(archivedNotes, notes, note), shallowEqual);
  };

  const editCallback = (noteIdx, propName, value) => {
    dispatch(editArchiveNote(archivedNotes, noteIdx, propName, value));
  };

  const editNote = (noteId) => history.push("/archive/" + noteId);

  return (
    <div className={`archive-container`}>
      {selectedNoteIndex >= 0 && (
        <EditNote
          archived={true}
          deleteNote={deleteNote}
          togglePin={togglePin}
          toggleArchive={toggleArchive}
          editCallback={editCallback}
          notes={archivedNotes}
          noteIdx={selectedNoteIndex}
        />
      )}
      <div className={`notes-container`}>
        {archivedNotes.map((note) => (
          <Note
            editNote={editNote}
            toggleArchive={toggleArchive}
            togglePin={togglePin}
            archived={true}
            deleteNote={deleteNote}
            key={note.id}
            data={note}
          />
        ))}
      </div>
      {!archivedNotes.length && (
        <div className={`empty-archive-container`}>
          <div className={`empty-archive`}>
            <FontAwesomeIcon className={`icon`} icon={faArchive} />
            <p className={`text`}>Your archived notes appear here</p>
          </div>
        </div>
      )}
    </div>
  );
}

export default Archive;
