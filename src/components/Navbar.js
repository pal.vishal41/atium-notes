import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { useLocation } from "react-router-dom";

import SearchBar from "./SearchBar";
import { toggleTheme, toggleSidebar } from "../actions";
import "../styles/navbar.scss";

function Navbar() {
  const theme = useSelector((state) => state.theme, shallowEqual);
  const sidebarCollapsed = useSelector(
    (state) => state.sidebarCollapsed,
    shallowEqual
  );
  const location = useLocation();
  const dispatch = useDispatch();

  const renderPageName = () => {
    if (location.pathname.includes("/archive")) return "Archive";
    if (location.pathname.includes("/search")) return "Search";
    else return "Notes";
  };

  return (
    <div className={`navbar ${theme}`}>
      <div className={`info`}>
        <FontAwesomeIcon
          onClick={() => dispatch(toggleSidebar(sidebarCollapsed))}
          className={`sidebar-button ${theme}`}
          icon={faBars}
        />
        <span className={`page-name`}>{renderPageName()}</span>
      </div>
      <SearchBar />
      <div
        onClick={() => dispatch(toggleTheme(theme))}
        className={`theme-selection`}
      >
        <FontAwesomeIcon
          className={`icon ${theme}`}
          icon={theme === "light" ? faMoon : faSun}
        />
      </div>
    </div>
  );
}

export default Navbar;
