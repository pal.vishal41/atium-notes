import { useEffect, useState, useRef } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark as faBookmarkRegular } from "@fortawesome/free-regular-svg-icons";
import {
  faBookmark,
  faArchive,
  faTrash,
  faImage,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { detect } from "detect-browser";
import { EditorState, convertToRaw } from "draft-js";
import "draft-js/dist/Draft.css";

import TextEditor from "./TextEditor";
import { addNote, addArchivedNote } from "../actions";
import "../styles/createNote.scss";

function CreateNote() {
  const [active, setActive] = useState(false);
  const [height, setHeight] = useState(50);
  const [title, setTitle] = useState("");
  const [pinned, setPinned] = useState(false);
  const [image, setImage] = useState("");
  const [id, setId] = useState("");
  const [uploadProgress, setUploadProgress] = useState(0);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [mobileText, setMobileText] = useState("");

  const inputRef = useRef();
  const editorRef = useRef();
  const dispatch = useDispatch();
  const theme = useSelector((state) => state.theme);
  const allNotes = useSelector((state) => state.notes);
  const allArchivedNotes = useSelector((state) => state.archivedNotes);
  const browser = detect();

  useEffect(() => {
    if (!active) {
      setHeight(50);
      setTitle("");
      setPinned(false);
      setImage("");
      setId("");
      setMobileText("");
      setEditorState(EditorState.createEmpty());
    }
  }, [active]);

  const activateNote = () => {
    setActive(true);
    setId(Date.now().toString());
    editorRef.current?.focus();
  };

  const resizeText = (e) => {
    setMobileText(e.target.value);
    if (e.target.scrollHeight > 50) {
      setHeight(e.target.scrollHeight);
    }
  };

  const closeCreation = () => {
    if (browser.os === "Android OS" || browser.os === "iOS") {
      if (title.trim() || mobileText.trim() || image) {
        dispatch(
          addNote(allNotes, {
            id,
            title,
            content: { editorObj: {}, rawText: mobileText },
            pinned,
            editedAt: id,
            image,
          }),
          shallowEqual
        );
      }
    } else {
      const editorObj = convertToRaw(editorState.getCurrentContent());
      const rawText = editorObj?.blocks.map((block) => block.text).join(" ");
      if (title.trim() || rawText.trim() || image) {
        dispatch(
          addNote(allNotes, {
            id,
            title,
            content: { editorObj, rawText },
            pinned,
            editedAt: id,
            image,
          }),
          shallowEqual
        );
      }
    }
    setActive(false);
  };

  const createArchivedNote = () => {
    if (browser.os === "Android OS" || browser.os === "iOS") {
      if (title.trim() || mobileText.trim() || image) {
        dispatch(
          addArchivedNote(allArchivedNotes, {
            id,
            title,
            content: { editorObj: {}, rawText: mobileText },
            pinned: false,
            editedAt: id,
            image,
          }),
          shallowEqual
        );
        setActive(false);
      }
    } else {
      const editorObj = convertToRaw(editorState.getCurrentContent());
      const rawText =
        editorObj?.blocks.map((block) => block.text).join(" ") || "";
      if (title.trim() || rawText.trim() || image) {
        dispatch(
          addArchivedNote(allArchivedNotes, {
            id,
            title,
            content: { editorObj, rawText },
            pinned: false,
            editedAt: id,
            image,
          }),
          shallowEqual
        );
        setActive(false);
      }
    }
  };

  const selectImage = (e) => {
    inputRef.current.click();
  };

  const uploadImage = async (e) => {
    const image = e.target.files[0];
    try {
      const data = new FormData();
      const fileExtension =
        image.name.split(".")[image.name.split(".").length - 1];
      data.append("filename", id + "." + fileExtension);
      data.append("image", image);
      const config = {
        onUploadProgress: (progressEvent) => {
          const percentCompleted = Math.floor(
            (progressEvent.loaded * 100) / progressEvent.total
          );
          setUploadProgress(percentCompleted);
        },
      };
      const res = await axios.post(
        "https://3u2ou8krn9.execute-api.ap-south-1.amazonaws.com",
        data,
        config
      );
      if (res?.data?.status === 200) {
        const imageUrl =
          "https://s3.ap-south-1.amazonaws.com/notes.mydn-images/" +
          id +
          "." +
          fileExtension +
          "?" +
          Date.now().toString();
        setImage(imageUrl);
      } else {
        alert(res?.data?.message || "Image Upload Failed");
      }
    } catch (e) {
      alert(e?.response?.data?.message || "Something Went Wrong");
    }
    setUploadProgress(0);
    inputRef.current.value = "";
  };

  const deleteImage = (e) => {
    setImage("");
  };

  return (
    <div className={`create-note-container ${theme}`}>
      {image && (
        <div className={`create-note-image`}>
          <div className={`resizable`}>
            <FontAwesomeIcon
              title="Delete Image"
              onClick={deleteImage}
              className={`image-delete-icon ${theme}`}
              icon={faTrash}
            />
            <img className={`image`} src={image} alt="" />
          </div>
        </div>
      )}

      {uploadProgress > 0 && (
        <div className={`progress-bar`}>
          <div
            style={{ width: uploadProgress + "%" }}
            className={`filled-bar`}
          />
        </div>
      )}

      {active && (
        <FontAwesomeIcon
          title={pinned ? "Unpin" : "Pin"}
          onClick={() => setPinned(!pinned)}
          className={`pin-icon ${theme}`}
          icon={pinned ? faBookmark : faBookmarkRegular}
        />
      )}

      {active && (
        <div className={`create-note-title`}>
          <input
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            type="text"
            placeholder="Title"
          />
        </div>
      )}
      <div className={`create-note-text`}>
        {browser.os === "Android OS" || browser.os === "iOS" ? (
          <textarea
            style={{ height }}
            onFocus={activateNote}
            onInput={resizeText}
            value={mobileText}
            placeholder="Take a note..."
          />
        ) : (
          <>
            {!active && (
              <textarea onFocus={activateNote} placeholder="Take a note..." />
            )}
            <TextEditor
              active={active}
              editorRef={editorRef}
              editorState={editorState}
              setEditorState={setEditorState}
            />
          </>
        )}
      </div>
      {active && (
        <div className={`create-note-actions`}>
          <div className={`icons`}>
            <input
              ref={inputRef}
              style={{ display: "none" }}
              onChange={uploadImage}
              type="file"
              accept="image/jpg,image/png"
            />
            {!image && (
              <FontAwesomeIcon
                title="Image"
                onClick={selectImage}
                className={`create-note-actions-icon ${theme}`}
                icon={faImage}
              />
            )}
            <FontAwesomeIcon
              title="Archive"
              onClick={createArchivedNote}
              className={`create-note-actions-icon ${theme}`}
              icon={faArchive}
            />
            <FontAwesomeIcon
              title="Delete"
              onClick={() => setActive(false)}
              className={`create-note-actions-icon ${theme}`}
              icon={faTrash}
            />
          </div>
          <div className={`buttons ${theme}`}>
            <button onClick={closeCreation}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default CreateNote;
