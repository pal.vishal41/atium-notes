import { useEffect, useState } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import {
  deleteNote as deleteNoteAction,
  deleteArchivedNote as deleteArchivedNoteAction,
  pinNote as pinNoteAction,
  pinArchivedNote as pinArchivedNoteAction,
  archiveNote as archiveNoteAction,
  unarchiveNote as unarchiveNoteAction,
  editNote as editNoteAction,
  editArchiveNote as editArchiveNoteAction,
} from "../actions";

import Note from "./Note";
import EditNote from "./EditNote";
import "../styles/searchResults.scss";

function SearchResults() {
  const notes = useSelector((state) => state.notes);
  const archivedNotes = useSelector((state) => state.archivedNotes);
  const [searchResult, setSearchResult] = useState({});
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const selectedNoteIndex = params.noteId
    ? notes.findIndex((n) => n.id === params.noteId)
    : -1;
  const selectedArchivedNoteIndex = params.noteId
    ? archivedNotes.findIndex((n) => n.id === params.noteId)
    : -1;
  const searchText = params.searchText.toLowerCase();

  useEffect(() => {
    let searchedNotes = notes.filter(
      (note) =>
        note.title.toLowerCase().includes(searchText) ||
        note.content.rawText?.toLowerCase().includes(searchText)
    );
    let searchedArchived = archivedNotes.filter(
      (note) =>
        note.title.toLowerCase().includes(searchText) ||
        note.content.rawText?.toLowerCase().includes(searchText)
    );
    setSearchResult({
      pinned: searchedNotes.filter((note) => note.pinned),
      unpinned: searchedNotes.filter((note) => !note.pinned),
      archived: searchedArchived,
    });
  }, [searchText, notes, archivedNotes]);

  useEffect(() => {
    if (
      params.noteId &&
      selectedNoteIndex === -1 &&
      selectedArchivedNoteIndex === -1
    )
      history.push("/search/" + searchText);
  });

  const deleteNote = (noteId) => {
    dispatch(deleteNoteAction(notes, noteId), shallowEqual);
  };

  const deleteArchivedNote = (noteId) => {
    dispatch(deleteArchivedNoteAction(archivedNotes, noteId), shallowEqual);
  };

  const pinNote = (note) => {
    dispatch(pinNoteAction(notes, note.id));
  };

  const pinArchivedNote = (note) => {
    dispatch(pinArchivedNoteAction(archivedNotes, notes, note));
  };

  const archiveNote = (note) => {
    dispatch(archiveNoteAction(archivedNotes, notes, note), shallowEqual);
  };

  const unarchiveNote = (note) => {
    dispatch(unarchiveNoteAction(archivedNotes, notes, note), shallowEqual);
  };

  const editCallback = (noteIdx, propName, value) => {
    dispatch(editNoteAction(notes, noteIdx, propName, value));
  };

  const editArchivedCallback = (noteIdx, propName, value) => {
    dispatch(editArchiveNoteAction(archivedNotes, noteIdx, propName, value));
  };

  const editNote = (noteId) =>
    history.push("/search/" + searchText + "/" + noteId);

  return (
    <div className={`search-results-container`}>
      {selectedNoteIndex >= 0 && (
        <EditNote
          deleteNote={deleteNote}
          togglePin={pinNote}
          toggleArchive={archiveNote}
          editCallback={editCallback}
          notes={notes}
          noteIdx={selectedNoteIndex}
        />
      )}
      {selectedArchivedNoteIndex >= 0 && (
        <EditNote
          deleteNote={deleteArchivedNote}
          togglePin={pinArchivedNote}
          toggleArchive={unarchiveNote}
          editCallback={editArchivedCallback}
          notes={archivedNotes}
          noteIdx={selectedArchivedNoteIndex}
        />
      )}
      {searchResult.pinned && searchResult.pinned.length ? (
        <>
          <div className={`category`}>
            <span>PINNED</span>
          </div>
          <div className={`notes-container`}>
            {searchResult.pinned.map((note) => (
              <Note
                editNote={editNote}
                toggleArchive={archiveNote}
                togglePin={pinNote}
                deleteNote={deleteNote}
                key={note.id}
                data={note}
              />
            ))}
          </div>
        </>
      ) : null}

      {searchResult.unpinned && searchResult.unpinned.length ? (
        <>
          <div className={`category`}>
            <span>NOTES</span>
          </div>
          <div className={`notes-container`}>
            {searchResult.unpinned.map((note) => (
              <Note
                editNote={editNote}
                toggleArchive={archiveNote}
                togglePin={pinNote}
                deleteNote={deleteNote}
                key={note.id}
                data={note}
              />
            ))}
          </div>
        </>
      ) : null}

      {searchResult.archived && searchResult.archived.length ? (
        <>
          <div className={`category`}>
            <span>ARCHIVED</span>
          </div>
          <div className={`notes-container`}>
            {searchResult.archived.map((note) => (
              <Note
                archived={true}
                editNote={editNote}
                toggleArchive={unarchiveNote}
                togglePin={pinArchivedNote}
                deleteNote={deleteArchivedNote}
                key={note.id}
                data={note}
              />
            ))}
          </div>
        </>
      ) : null}
    </div>
  );
}

export default SearchResults;
