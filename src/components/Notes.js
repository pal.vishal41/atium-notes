import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { useEffect } from "react";

import Note from "./Note";
import CreateNote from "./CreateNote";
import EditNote from "./EditNote";

import {
  deleteNote as deleteNoteAction,
  pinNote,
  archiveNote,
  editNote as editNoteAction,
} from "../actions";
import "../styles/notes.scss";

function Notes() {
  const notes = useSelector((state) => state.notes);
  const archivedNotes = useSelector((state) => state.archivedNotes);
  const dispatch = useDispatch();
  const pinnedNotes = notes.filter((note) => note.pinned);
  const unpinnedNotes = notes.filter((note) => !note.pinned);
  const history = useHistory();
  const params = useParams();
  const selectedNoteIndex = params.noteId
    ? notes.findIndex((n) => n.id === params.noteId)
    : -1;

  useEffect(() => {
    if (params.noteId && selectedNoteIndex === -1) history.push("/");
  });

  const deleteNote = (noteId) => {
    dispatch(deleteNoteAction(notes, noteId), shallowEqual);
  };

  const togglePin = (note) => {
    dispatch(pinNote(notes, note.id));
  };

  const toggleArchive = (note) => {
    dispatch(archiveNote(archivedNotes, notes, note), shallowEqual);
  };

  const editCallback = (noteIdx, propName, value) => {
    dispatch(editNoteAction(notes, noteIdx, propName, value));
  };

  const editNote = (noteId) => history.push("/" + noteId);

  return (
    <div className={`home-container`}>
      {selectedNoteIndex >= 0 && (
        <EditNote
          deleteNote={deleteNote}
          togglePin={togglePin}
          toggleArchive={toggleArchive}
          editCallback={editCallback}
          notes={notes}
          noteIdx={selectedNoteIndex}
        />
      )}
      <CreateNote />
      {pinnedNotes.length ? (
        <div className={`category`}>
          <span>PINNED</span>
        </div>
      ) : null}
      <div className={`notes-container pinned`}>
        {pinnedNotes.map((note) => (
          <Note
            editNote={editNote}
            toggleArchive={toggleArchive}
            togglePin={togglePin}
            deleteNote={deleteNote}
            key={note.id}
            data={note}
          />
        ))}
      </div>
      {pinnedNotes.length && unpinnedNotes.length ? (
        <div className={`category`}>
          <span>OTHERS</span>
        </div>
      ) : null}
      <div className={`notes-container unpinned`}>
        {unpinnedNotes.map((note) => (
          <Note
            editNote={editNote}
            toggleArchive={toggleArchive}
            togglePin={togglePin}
            deleteNote={deleteNote}
            key={note.id}
            data={note}
          />
        ))}
      </div>
    </div>
  );
}

export default Notes;
