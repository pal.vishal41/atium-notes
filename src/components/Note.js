import { useState } from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark as faBookmarkRegular } from "@fortawesome/free-regular-svg-icons";
import { Editor, EditorState, convertFromRaw } from "draft-js";
import { detect } from "detect-browser";
import {
  faBookmark,
  faArchive,
  faTrash,
  faStickyNote,
} from "@fortawesome/free-solid-svg-icons";
import "draft-js/dist/Draft.css";

import "../styles/note.scss";

function Note({
  data,
  deleteNote,
  togglePin,
  toggleArchive,
  archived,
  editNote,
}) {
  const [actions, setActions] = useState(false);
  const theme = useSelector((state) => state.theme);
  const showActions = () => {
    if (!actions) setActions(true);
  };
  const browser = detect();
  const editorState = data.content?.editorObj?.blocks
    ? EditorState.createWithContent(convertFromRaw(data.content.editorObj))
    : EditorState.createEmpty();
  return (
    <div
      onMouseOver={showActions}
      onPointerEnter={showActions}
      onPointerLeave={() => setActions(false)}
      onClick={() => editNote(data.id)}
      className={`note-container`}
    >
      {data.image && (
        <div className={`note-image-container`}>
          <img className={`note-image`} src={data.image} alt="" />
        </div>
      )}
      <FontAwesomeIcon
        onClick={(e) => {
          e.stopPropagation();
          togglePin(data);
        }}
        title={data.pinned ? "Unpin" : "Pin"}
        className={`pin-icon ${theme} ${actions ? "" : "hidden"}`}
        icon={data.pinned ? faBookmark : faBookmarkRegular}
      />
      {data.title && (
        <div className={`note-title`}>
          <span>{data.title}</span>
        </div>
      )}
      <div className={`note-text`}>
        {browser.os === "Android OS" || browser.os === "iOS" ? (
          <p>
            {data.content.rawText.slice(0, 200)}
            {data.content.rawText.length > 200 ? "..." : ""}
          </p>
        ) : (
          <Editor readOnly editorState={editorState} />
        )}
      </div>
      <div className={`note-actions`}>
        <FontAwesomeIcon
          onClick={(e) => {
            e.stopPropagation();
            toggleArchive(data);
          }}
          title={archived ? "Unarchive" : "Archive"}
          className={`note-actions-icon ${theme} ${actions ? "" : "hidden"}`}
          icon={archived ? faStickyNote : faArchive}
        />
        <FontAwesomeIcon
          onClick={(e) => {
            e.stopPropagation();
            deleteNote(data.id);
          }}
          title="Delete"
          className={`note-actions-icon ${theme} ${actions ? "" : "hidden"}`}
          icon={faTrash}
        />
      </div>
    </div>
  );
}

export default Note;
