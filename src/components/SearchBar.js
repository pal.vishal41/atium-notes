import { useSelector, shallowEqual } from "react-redux";
import { useState, useRef, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";

import "../styles/searchBar.scss";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function SearchBar() {
  const theme = useSelector((state) => state.theme, shallowEqual);
  const history = useHistory();
  const location = useLocation();
  const [searchText, setSearchText] = useState(
    location.pathname.split("/").length >= 3
      ? location.pathname.split("/")[2]
      : ""
  );
  const prevSearchText = usePrevious(searchText);
  let timer = useRef(null);

  useEffect(() => {
    clearTimeout(timer.current);
    timer.current = setTimeout(() => {
      showSearch();
    }, 400);
  });

  useEffect(() => {
    if (!location.pathname.includes("/search")) setSearchText("");
  }, [location]);

  const showSearch = () => {
    if (searchText.trim().length >= 3 && prevSearchText !== searchText) {
      history.push("/search/" + searchText);
    }
  };

  const handleChange = (e) => {
    setSearchText(e.target.value);
  };

  const clearSearch = () => {
    setSearchText("");
    history.push("/");
  };

  return (
    <div className={`search-container ${theme}`}>
      <div onClick={showSearch} className={`search-button ${theme}`}>
        <FontAwesomeIcon className={`search-icon`} icon={faSearch} />
      </div>
      <div className={`search-box`}>
        <input
          className={`search-input`}
          onChange={handleChange}
          value={searchText}
          placeholder="Search"
          type="text"
        />
      </div>
      {(searchText || location.pathname.includes("/search")) && (
        <div onClick={clearSearch} className={`search-close`}>
          <FontAwesomeIcon className={`search-icon`} icon={faTimes} />
        </div>
      )}
    </div>
  );
}

export default SearchBar;
