import { useSelector, shallowEqual } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArchive, faStickyNote } from "@fortawesome/free-solid-svg-icons";
import { useHistory, useLocation } from "react-router-dom";

import "../styles/sidebar.scss";

function Sidebar() {
  const theme = useSelector((state) => state.theme, shallowEqual);
  const sidebarCollapsed = useSelector(
    (state) => state.sidebarCollapsed,
    shallowEqual
  );
  const history = useHistory();
  const location = useLocation();
  return (
    <div
      className={`sidebar ${theme} ${
        sidebarCollapsed === "true" ? "collapsed" : ""
      }`}
    >
      <div
        onClick={() => history.push("/")}
        className={`sidebar-link ${
          location.pathname === "/" ? "active" : "inactive"
        }`}
      >
        <FontAwesomeIcon className={`sidebar-link-icon`} icon={faStickyNote} />
        <span className={`sidebar-link-text`}>Notes</span>
      </div>
      <div
        onClick={() => history.push("/archive")}
        className={`sidebar-link ${
          location.pathname === "/archive" ? "active" : "inactive"
        }`}
      >
        <FontAwesomeIcon className={`sidebar-link-icon`} icon={faArchive} />
        <span className={`sidebar-link-text`}>Archive</span>
      </div>
    </div>
  );
}

export default Sidebar;
