import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { useState, useRef, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark as faBookmarkRegular } from "@fortawesome/free-regular-svg-icons";
import {
  faBookmark,
  faArchive,
  faTrash,
  faStickyNote,
  faImage,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { detect } from "detect-browser";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import "draft-js/dist/Draft.css";

import TextEditor from "./TextEditor";

import "../styles/editNote.scss";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function EditNote({
  notes,
  noteIdx,
  editCallback,
  toggleArchive,
  togglePin,
  deleteNote,
  archived,
}) {
  const history = useHistory();
  const theme = useSelector((state) => state.theme);
  const selectedNote = notes[noteIdx];
  const inputRef = useRef();
  const [uploadProgress, setUploadProgress] = useState(0);
  const [height, setHeight] = useState(100);
  const [editorState, setEditorState] = useState(() =>
    selectedNote.content?.editorObj?.blocks
      ? EditorState.createWithContent(
          convertFromRaw(selectedNote.content.editorObj)
        )
      : EditorState.createEmpty()
  );
  const prevEditorState = usePrevious(editorState);
  const browser = detect();

  useEffect(() => {
    if (
      browser.os !== "Android OS" &&
      browser.os !== "iOS" &&
      editorState !== prevEditorState
    ) {
      const editorObj = convertToRaw(editorState.getCurrentContent());
      const rawText = editorObj?.blocks.map((block) => block.text).join(" ");
      editCallback(noteIdx, "content", { editorObj, rawText });
    }
  }, [editorState, editCallback, noteIdx, prevEditorState, browser]);

  const resizeContent = (e) => {
    editCallback(noteIdx, "content", {
      editorObj: {},
      rawText: e.target.value,
    });
    if (e.target.scrollHeight > 50) {
      setHeight(e.target.scrollHeight);
    }
  };

  const selectImage = (e) => {
    inputRef.current.click();
  };

  const uploadImage = async (e) => {
    const image = e.target.files[0];
    if (image.size > 4194304) {
      alert("Please upload an image with size smaller than 4MB");
      return;
    }
    try {
      const data = new FormData();
      const fileExtension =
        image.name.split(".")[image.name.split(".").length - 1];
      data.append("filename", selectedNote.id + "." + fileExtension);
      data.append("image", image);
      const config = {
        onUploadProgress: (progressEvent) => {
          const percentCompleted = Math.floor(
            (progressEvent.loaded * 100) / progressEvent.total
          );
          setUploadProgress(percentCompleted);
        },
      };
      const res = await axios.post(
        "https://3u2ou8krn9.execute-api.ap-south-1.amazonaws.com",
        data,
        config
      );
      if (res?.data?.status === 200) {
        const imageUrl =
          "https://s3.ap-south-1.amazonaws.com/notes.mydn-images/" +
          selectedNote.id +
          "." +
          fileExtension +
          "?" +
          Date.now().toString();
        editCallback(noteIdx, "image", imageUrl);
      } else {
        alert(res?.data?.message || "Image Upload Failed");
      }
    } catch (e) {
      alert(e?.response?.data?.message || "Something Went Wrong");
    }
    setUploadProgress(0);
    inputRef.current.value = "";
  };

  const deleteImage = (e) => {
    editCallback(noteIdx, "image", "");
  };

  const handleChange = (e) => {
    editCallback(noteIdx, e.target.name, e.target.value);
  };

  const closeModal = () => {
    history.goBack();
  };

  return (
    <div onClick={closeModal} className={`edit-note-container`}>
      <div onClick={(e) => e.stopPropagation()} className={`note ${theme}`}>
        {selectedNote.image && (
          <div className="note-image-container">
            <div className={`resizable`}>
              <FontAwesomeIcon
                title="Delete Image"
                onClick={deleteImage}
                className={`image-delete-icon ${theme}`}
                icon={faTrash}
              />
              <img className={`note-image`} src={selectedNote.image} alt="" />
            </div>
          </div>
        )}
        {uploadProgress > 0 && (
          <div className={`progress-bar`}>
            <div
              style={{ width: uploadProgress + "%" }}
              className={`filled-bar`}
            />
          </div>
        )}
        <FontAwesomeIcon
          onClick={() => togglePin(selectedNote)}
          className={`pin-icon ${theme}`}
          icon={selectedNote.pinned ? faBookmark : faBookmarkRegular}
        />
        <div className={`note-title`}>
          <input
            onChange={handleChange}
            name="title"
            type="text"
            placeholder="Title"
            value={selectedNote.title}
          />
        </div>
        <div className={`note-content`}>
          {browser.os === "Android OS" || browser.os === "iOS" ? (
            <textarea
              name="content"
              onInput={resizeContent}
              style={{ height }}
              value={selectedNote.content.rawText}
              placeholder="Take a note..."
            />
          ) : (
            <TextEditor
              active={true}
              editorState={editorState}
              setEditorState={setEditorState}
            />
          )}
        </div>
        <div className={`note-actions`}>
          <div className={`note-actions-icons`}>
            <input
              ref={inputRef}
              style={{ display: "none" }}
              onChange={uploadImage}
              type="file"
              accept="image/jpg,image/png"
            />
            {!selectedNote.image && (
              <FontAwesomeIcon
                onClick={selectImage}
                title="Image"
                className={`note-actions-icon ${theme}`}
                icon={faImage}
              />
            )}
            <FontAwesomeIcon
              onClick={() => {
                toggleArchive(selectedNote);
                closeModal();
              }}
              className={`note-actions-icon ${theme}`}
              icon={archived ? faStickyNote : faArchive}
            />
            <FontAwesomeIcon
              onClick={() => {
                deleteNote(selectedNote.id);
                closeModal();
              }}
              className={`note-actions-icon ${theme}`}
              icon={faTrash}
            />
          </div>
          <div
            onClick={closeModal}
            className={`note-actions-buttons  ${theme}`}
          >
            <button>Close</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditNote;
