import { Switch, Route } from "react-router-dom";
import { useSelector, shallowEqual } from "react-redux";

import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Notes from "./components/Notes";
import Archive from "./components/Archive";
import SearchResults from "./components/SearchResults";

import "./styles/app.scss";

function App() {
  const theme = useSelector((state) => state.theme, shallowEqual);
  return (
    <div className={`App ${theme}`}>
      <Navbar />
      <div className={`content-container`}>
        <Sidebar />
        <div className={`content`}>
          <Switch>
            <Route path="/archive/:noteId?">
              <Archive />
            </Route>
            <Route path="/search/:searchText/:noteId?">
              <SearchResults />
            </Route>
            <Route path="/:noteId?">
              <Notes />
            </Route>
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default App;
