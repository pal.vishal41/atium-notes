import { actionTypes } from "../actions";

const initialState = {
  notes: JSON.parse(localStorage.getItem("notes")) || [],
  archivedNotes: JSON.parse(localStorage.getItem("archivedNotes")) || [],
  sidebarCollapsed: localStorage.getItem("sidebarState") || "true",
  theme: localStorage.getItem("theme") || "dark",
};

const notesReducer = (state = initialState, { type, payload }) => {
  const {
    TOGGLE_THEME,
    TOGGLE_SIDEBAR,
    SET_NOTES,
    SET_ARCHIVED_NOTES,
    SET_ALL_NOTES,
  } = actionTypes;
  switch (type) {
    case TOGGLE_THEME:
      return { ...state, theme: state.theme === "light" ? "dark" : "light" };
    case TOGGLE_SIDEBAR:
      return { ...state, sidebarCollapsed: payload.sidebarCollapsed };
    case SET_NOTES:
      return { ...state, notes: payload.notes };
    case SET_ARCHIVED_NOTES:
      return { ...state, archivedNotes: payload.archivedNotes };
    case SET_ALL_NOTES:
      return {
        ...state,
        notes: payload.notes,
        archivedNotes: payload.archivedNotes,
      };
    default:
      return state;
  }
};

export default notesReducer;
