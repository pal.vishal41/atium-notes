export const actionTypes = {
  TOGGLE_SIDEBAR: "TOGGLE_SIDEBAR",
  TOGGLE_THEME: "TOGGLE_THEME",
  SET_NOTES: "SET_NOTES",
  SET_ARCHIVED_NOTES: "SET_ARCHIVED_NOTES",
  SET_ALL_NOTES: "SET_ALL_NOTES",
};

export const toggleSidebar = (currentState) => {
  let sidebarCollapsed = currentState === "true" ? "false" : "true";
  localStorage.setItem("sidebarState", sidebarCollapsed);
  return { type: actionTypes.TOGGLE_SIDEBAR, payload: { sidebarCollapsed } };
};

export const toggleTheme = (currentTheme) => {
  localStorage.setItem("theme", currentTheme === "light" ? "dark" : "light");
  return { type: actionTypes.TOGGLE_THEME };
};

export const addNote = (allNotes, note) => {
  let notes = [note, ...allNotes];
  localStorage.setItem("notes", JSON.stringify(notes));
  return { type: actionTypes.SET_NOTES, payload: { notes } };
};

export const deleteNote = (allNotes, noteId) => {
  let notes = allNotes.filter((note) => note.id !== noteId);
  localStorage.setItem("notes", JSON.stringify(notes));
  return { type: actionTypes.SET_NOTES, payload: { notes } };
};

export const addArchivedNote = (allArchived, note) => {
  let archivedNotes = [note, ...allArchived];
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ARCHIVED_NOTES, payload: { archivedNotes } };
};

export const deleteArchivedNote = (allArchived, noteId) => {
  let archivedNotes = allArchived.filter((note) => note.id !== noteId);
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ARCHIVED_NOTES, payload: { archivedNotes } };
};

export const pinNote = (allNotes, noteId) => {
  let notes = allNotes.map((note) =>
    note.id === noteId ? { ...note, pinned: !note.pinned } : note
  );
  localStorage.setItem("notes", JSON.stringify(notes));
  return { type: actionTypes.SET_NOTES, payload: { notes } };
};

export const pinArchivedNote = (allArchived, allNotes, note) => {
  let archivedNotes = allArchived.filter((n) => n.id !== note.id);
  let notes = [{ ...note, pinned: true }, ...allNotes];
  localStorage.setItem("notes", JSON.stringify(notes));
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ALL_NOTES, payload: { archivedNotes, notes } };
};

export const archiveNote = (allArchived, allNotes, note) => {
  let notes = allNotes.filter((n) => n.id !== note.id);
  let archivedNotes = [{ ...note, pinned: false }, ...allArchived];
  localStorage.setItem("notes", JSON.stringify(notes));
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ALL_NOTES, payload: { archivedNotes, notes } };
};

export const unarchiveNote = (allArchived, allNotes, note) => {
  let archivedNotes = allArchived.filter((n) => n.id !== note.id);
  let notes = [note, ...allNotes];
  localStorage.setItem("notes", JSON.stringify(notes));
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ALL_NOTES, payload: { archivedNotes, notes } };
};

export const editNote = (allNotes, noteIdx, propName, value) => {
  let notes = [...allNotes];
  notes[noteIdx] = { ...notes[noteIdx], [propName]: value };
  localStorage.setItem("notes", JSON.stringify(notes));
  return { type: actionTypes.SET_NOTES, payload: { notes } };
};

export const editArchiveNote = (allArchived, noteIdx, propName, value) => {
  let archivedNotes = [...allArchived];
  archivedNotes[noteIdx] = { ...archivedNotes[noteIdx], [propName]: value };
  localStorage.setItem("archivedNotes", JSON.stringify(archivedNotes));
  return { type: actionTypes.SET_ARCHIVED_NOTES, payload: { archivedNotes } };
};
